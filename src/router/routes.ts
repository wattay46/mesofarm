import { RouteConfig } from 'vue-router'

const routes: RouteConfig[] = [
  {
    path: '/',
    component: () => import('layouts/HomeLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/welfare', component: () => import('pages/WelFare.vue') },
      { path: '/financials', component: () => import('pages/Financials.vue') },
      { path: '/projects', component: () => import('pages/Projects.vue') },
      { path: '/profile', component: () => import('pages/Profile.vue') }
    ]
  },

  {
    path: '/welfare',
    component: () => import('layouts/WelFareLayout.vue'),
    children: [
      { path: '/welfare/leaves', component: () => import('pages/WelfareLeaves.vue') },
      { path: '/welfare/sicksheets', component: () => import('pages/WelfareSickSheets.vue') },
      { path: '/welfare/myfamily', component: () => import('pages/WelfareMyFamily.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
